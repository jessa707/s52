// To use react bootstrap components, you must first import them from the react-bootstrap package
import {Container, Navbar, Nav} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import { Fragment,useState } from 'react'

export default function AppNavbar(){
  
const [user, setUser] = useState(localStorage.getItem('email'))
return(
    <Navbar bg="light" expand="lg">
          <Container fluid>
            <Navbar.Brand as={Link} to={NavLink}>Zuitt Booking</Navbar.Brand>


            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

                { (user !== null) ?

                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  :
              <Fragment>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link> 
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </Fragment>
                  

                }
              </Nav>
            </Navbar.Collapse>
          </Container>
      </Navbar>
  )
}
