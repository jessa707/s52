import { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function Login(event) {
    event.preventDefault()

    // Set the email of the Authenticated user in the local storage
    localStorage.setItem('email', email)

    // For clearing out the form
    setEmail('');
    setPassword('');

    alert('Thank you for registering!');
  }

  useEffect(() => {
    // All input fields must not be empty
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (

    <Form onSubmit={(event) => Login(event)}>
    <h1>Login Page</h1>
      <Row>
        <Col md={6}>
          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              required
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>
        </Col>

        <Col md={6}>
          <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              required
            />
          </Form.Group>
        </Col>
      </Row>

      <Row>
        <Col>
          <Button variant="primary" type="submit" id="submitBtn" disabled={!isActive}>
            Submit
          </Button>
        </Col>
      </Row>
    </Form>
  );
}
