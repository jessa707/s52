import { Fragment } from 'react';
import { Link } from 'react-router-dom';

function NotFound() {
  return (
    <Fragment>
      <h1>Not Found</h1>
      <p>Page not found.</p>
      <Link to="/">Go back</Link>
    </Fragment>
  );
}

export default NotFound;
