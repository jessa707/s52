import { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Logout from './pages/Logout.js';
import Login from './pages/Login.js';
import NotFound from './pages/NotFound.js'; // Create a NotFound component for the "Not Found" page

function App() {
  return (
    <Router>
      <Container fluid>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/notfound" element={<NotFound />} /> {/* Handle undefined routes */}
        </Routes>
      </Container>
    </Router>
  );
}

export default App;
