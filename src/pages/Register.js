import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap'

export default function Register(){
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()

		// For clearing out the form
		setEmail('')
		setPassword1('')
		setPassword2('')

		// Show an alert
		alert("Thank you for registering!")
	}

	// For form validation, we use the 'useEffect' to track the values of the input fields and run a validation condition everytime there is user input in those fields.
	useEffect(() => {
		// All input fields must not be empty and the password/verify passwords fields must match in values.
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2])

	return(
		<Form onSubmit={(event) => registerUser(event)}>
		<h1> Register page </h1>
	        <Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	        {/* 2-way Data Binding is when the value of the state reflects on the value of the input field, and vice-versa. The way to implement it is by using an 'onChange' event listener on the input field and updating the value of the state to the current value of the input field. */}
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={event => setEmail(event.target.value)}
	                required
	            />
	            <Form.Text className="text-muted">
	                We'll never share your email with anyone else.
	            </Form.Text>
	        </Form.Group>

	        <Form.Group controlId="password1">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value={password1}
	                onChange={event => setPassword1(event.target.value)} 
	                required
	            />
	        </Form.Group>

	        <Form.Group controlId="password2">
	            <Form.Label>Verify Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                value={password2}
	                onChange={event => setPassword2(event.target.value)}
	                required
	            />
	        </Form.Group>

	    	{/* Conditional Rendering is when you render elements depending on a specific logical condition */}
	        { isActive ?
	        	<Button variant="success" type="submit" id="submitBtn">
		        	Submit
		        </Button>
		      	:
		      	<Button disabled variant="danger" type="submit" id="submitBtn">
		        	Submit
		        </Button>
	        }
	    </Form>
	)
}

