import React, { useState, useEffect } from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

export default function CourseCard({ course }) {
  const { name, description, price } = course;
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(10);
  const [femaleCount, setFemaleCount] = useState(0);
  const [maleCount, setMaleCount] = useState(0);
 const [isOpen, setIsOpen] = useState(true)

  //MINI Activity 
  /*create a 'seats' state which will represent the number of availabe seats in the course
  Put the value seats state in the card itself
  Everytime the 'enroll' button is clicked, the seats must go down in the value by 1
  Everytime the 'enroll' button is clicked, some Male and female gender is counted
*/

  function enroll() {
    if (seats > 0) {
      setCount(count + 1);
      setSeats(seats - 1);

      // Increment the appropriate gender count
      if (count % 2 === 0) {
        setFemaleCount(femaleCount + 1);
      } else {
        setMaleCount(maleCount + 1);
      }
    }
  }
//useEffect has 2 arguments,an aarow function and an array.The array will hold the specifiv state that you want to observe changes for.
  useEffect(() =>{

    if(seats === 0){
      setIsOpen(false)

    }
  },[seats])

  return (
    <Row className="my-2">
      <Col xs={12} md={6}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className="mb-1 text-muted">Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>

            <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
            <Card.Text>Php{price}</Card.Text>

            <Card.Text>Seats available: {seats}</Card.Text>
            <Card.Subtitle className="mb-2 text-muted">Enrollees:</Card.Subtitle>
            <Card.Text>Total: {count}</Card.Text>

            <Card.Text>Gender: {count % 2 === 0 ? 'Female' : 'Male'}</Card.Text>

            <Card.Subtitle className="mb-2 text-muted">Female:</Card.Subtitle>
            <Card.Text>Number: {femaleCount}</Card.Text>

            <Card.Subtitle className="mb-2 text-muted">Male:</Card.Subtitle>
            <Card.Text>Number: {maleCount}</Card.Text>

             <Card.Subtitle className="mb-2 text-muted">Course is Open:</Card.Subtitle>
            <Card.Text>{isOpen ? 'Yes' : 'No'}</Card.Text>


            <Button variant="primary" onClick={enroll} disabled={seats === 0}>
              Enroll
            </Button>

          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  }).isRequired
};
