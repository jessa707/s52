import{Fragment} from 'react'
import coursesData from '../data/courses.js'
import CourseCard from '../components/CourseCard.js'

export default function Courses(){
const courses = coursesData.map(course =>{
	return(
		

			<CourseCard key={course.id} course={course}/>
		
		)
})
	return(
		<Fragment>



		{courses}
		</Fragment>
		)
}